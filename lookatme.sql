
drop database if exists lookatme;
create database lookatme character set UTF8mb4 collate utf8mb4_bin;
use lookatme;

drop table if exists userLocation;
drop table if exists smartScreens;
drop table if exists adDigitalBoards;
drop table if exists adPictures;
drop table if exists userAds;
drop table if exists userPreferences;
drop table if exists matchingAds;
drop table if exists advertisements;
drop table if exists categories;
drop table if exists advertiserRank;
drop table if exists consumerNearDigitalBoard;
drop table if exists users;
drop table if exists digitalBoards;
drop table if exists cities;




create table cities
(
    id int auto_increment primary key,
    city varchar(80) not null,
    location point not null
);

create table digitalBoards
(
	id int auto_increment primary key,
    cityId int not null,
    location point not null,
    address varchar(150)  default 'לא מוגדר',
	foreign key(cityId) references cities(id) on update cascade on delete cascade
);

create table users
(
        email varchar(255) not null primary key,
        firstName varchar(50) not null,
        lastName varchar(50) not null,
        pictureUrl varchar(300) default '',
        advertiser TINYINT(1) default 0,
        administrator TINYINT(1) default 0,
        consumer TINYINT(1) default 0,
        premium TINYINT(1) default 0,
        userPassword varchar(30) not null
      
);

create table consumerNearDigitalBoard
(
    email varchar(255) not null,
       digitalBoardId  int not null,
foreign key(email) references users(email) on update cascade on delete cascade,
        foreign key(digitalBoardId) references digitalBoards(id) on update cascade on delete cascade,
        primary key(email,digitalBoardId)

);

create table advertiserRank
(
        advertiserEmail varchar(255) not null,
        consumerEmail varchar(255) not null,
        rank int not null default 0,
        foreign key(advertiserEmail) references users(email) on update cascade on delete cascade,
        foreign key(consumerEmail) references users(email) on update cascade on delete cascade,
        primary key(advertiserEmail,consumerEmail)
);

create table userLocation
(
    userEmail varchar(255) not null primary key,
    location point not null,
    updateTime DATETIME  DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    foreign key(userEmail) references users(email) on update cascade on delete cascade
);



create table categories
(
    id int auto_increment primary key,
    description varchar(100) not null UNIQUE
);





create table advertisements
(
		id int auto_increment primary key,
		userEmail varchar(255) not null,
                placeName varchar(50) default 'name',
                contactPhoneNumber varchar(50) not null,
		price float not null,
		serviceAddress varcharacter(100) default '',
		servicePoint point default null,
		serviceDescription varchar(300) not null default '',
		categoryId int not null,
		foreign key(userEmail) references users(email) on update cascade on delete cascade,
		foreign key(categoryId) references categories(id) on update cascade on delete cascade
);

create table matchingAds
(
	userEmail varchar(255) not null,
        advertisementId int not null,
        weight FLOAT default 0,
        viewed TINYINT(1) default 0,
        foreign key(userEmail) references users(email) on update cascade on delete cascade,
        foreign key(advertisementId) references advertisements(id) on update cascade on delete cascade,
        primary key(userEmail,advertisementId)
);

create table userAds
(
	 userEmail varchar(255) not null,
         advertisementId int not null,
         foreign key(userEmail) references users(email) on update cascade on delete cascade,
         foreign key(advertisementId) references advertisements(id) on update cascade on delete cascade,
         primary key(userEmail,advertisementId)
);


create table userPreferences
(
	   userEmail varchar(255) not null,
	   categoryId int not null,
       rank int default 1,
	   foreign key(categoryId) references categories(id) on update cascade on delete cascade,
	   foreign key(userEmail) references users(email) on update cascade on delete cascade,
		primary key(userEmail,categoryId)
);

create table adPictures
(
		id int auto_increment primary key,
		advertisementId int not null,
		pictureUrl varchar(300) default '',
		foreign key(advertisementId) references advertisements(id) on update cascade on delete cascade
);

create table adDigitalBoards
(
		advertisementId int not null,
		digitalBoardId  int not null,
		foreign key(advertisementId) references advertisements(id) on update cascade on delete cascade,
        foreign key(digitalBoardId) references digitalBoards(id) on update cascade on delete cascade,
		primary key(advertisementId,digitalBoardId)
);





create table smartScreens
(
		digitalBoardId  int not null,
		privateIp varchar(50) not null,
        lastModify datetime not null default now(),
        foreign key(digitalBoardId) references digitalBoards(id) on update cascade on delete cascade,
		primary key(digitalBoardId,privateIp)
);



insert into cities(city,location) values('תל אביב',POINT(32.064473, 34.775205));
insert into digitalBoards(cityId, location) VALUES ((select cities.id from cities where cities.city='תל אביב'),POINT(32.074355, 34.775899));
-- select ST_X(digitalBoards.location) from digitalBoards;
-- select * from digitalBoards;
insert into users(email,firstName,lastName,advertiser,userPassword,administrator,consumer) values ('nira7008@gmail.com','nir','adler',1,'70087008',1,1);
insert into users(email,firstName,lastName,advertiser,userPassword,administrator,consumer) values ('nataliemah67@gmail.com','נטלי','מחמלי',1,'Nm190793',1,1);
insert into users(email,firstName,lastName,advertiser,userPassword,administrator,consumer) values ('guy@gmail.com','guy','dani',0,'60056005',0,0);

